#include "Wtv020sd16p.h"


#define _DEBUG


#ifdef _DEBUG
	#define LOGINIT Serial.begin(9600)
	#define LOG(a) Serial.print(a)
	#define LOGLN(a) Serial.println(a)
#else
	#define LOGINIT
	#define LOG(a)
	#define LOGLN(a)
#endif

#define LEN 20
#define RECOUNTFRAMES 50
const float MMIN = 190.0f*LEN;
const float MMAX = 460.0f*LEN;

int distPin = 0; //analog IN
const int eyePin = 3;

const int resetPin = 2;  // The pin number of the reset pin.
const int clockPin = 4;  // The pin number of the clock pin.
const int dataPin = 5;  // The pin number of the data pin.
const int busyPin = 6;  // The pin number of the busy pin.

float soundStart = -1.0f;
float time = 0.0f;
int frame = 0;

void(* reset) (void) = 0;

Wtv020sd16p wtv020sd16p(resetPin,clockPin,dataPin,busyPin);

void sound() {
	if(soundStart < 0) {
		LOGLN("sound");
		soundStart = time;
		setEyes(1.0f);
		wtv020sd16p.asyncPlayVoice(0);
	}
}
void unsound() {
	if(soundStart >= 0 && time - soundStart >= 0.1f && !wtv020sd16p.isBusy() ) {
		LOGLN("unsound");
		soundStart = -1.0f;
		setEyes(0.0f);
		wtv020sd16p.stopVoice();
	}
}

float distMeter()
{
	static int values[LEN];
	static int frame = 0;
	static bool init = false;
	static float dist = 0.0f;
	
	values[(frame++)%LEN] = analogRead(distPin);
	
	if ( init && !(frame%RECOUNTFRAMES)){
		int v = 0;
		for(int i = 0; i < LEN; i ++ ) {
			v += values[i];
		}
		char buffer[0x100];
		dist = (v-MMIN)/(MMAX-MMIN);
		if (dist < 0.0f) {
			dist = 0.0f;
		}
		if (dist > 1.0f) {
			dist = 1.0f;
		}
		
		LOG(v/LEN);
		LOG(" ");
		LOGLN(dist);
	
	} else if(frame>=LEN) {
		init = true;
	}
	return dist;
}

void setEyes(float brightness) {
	analogWrite(eyePin, 255.0f*brightness);
}

void setup() {
	LOGINIT;
	wtv020sd16p.reset();
	pinMode(eyePin, OUTPUT);
	LOGLN("init");
}

void loop(){
	time = (float)millis()/1000.0f;
	frame ++;
	
	float dist = distMeter();
	if(dist>0.05f) {
		sound();
	}
	unsound();
	if (soundStart>=0.0f) {
		float t = (time - soundStart)*30.0f;
		setEyes((3+sin(t) + cos(2*t+1))/6);
	}
	delay(10);
	if (time > 100.0f) {
		reset();
	}
	if(!(frame%100)) {
		LOG("tick ");
		LOG(frame);
		LOG(" ");
		LOGLN(time);
	}
}

